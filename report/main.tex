\documentclass[
paper=a4,
fontsize=11pt,
parskip=half,        % vertical inter-paragraph spacing instead of indentation
DIV=12,              % "stripes" for layout definition
BCOR=10mm,           % binding correction
headings=normal,
appendixprefix=true, % Put "Appendix A" at the beginning...
open=any             % New chapter may begin on any page
]{scrbook}
%\usepackage[utf8]{inputenc}
%\usepackage[square,sort&compress]{natbib}
\usepackage{graphicx}
\usepackage{amsmath,amstext,amssymb,bm}
%\usepackage{setspace}
\usepackage{siunitx}
%\usepackage{verbatim}
%\usepackage{booktabs}
%\usepackage{lastpage}
\usepackage{pdfpages}
%\usepackage{url}
%\usepackage{tikz}
%\usetikzlibrary{shapes,arrows}
%\urlstyle{sf}
%\usepackage{rotating}
%\usepackage[numbers,square]{natbib}


\setcounter{page}{2}  % Page one is added manually, start counting at 2.
\usepackage{scrpage2}
\pagestyle{scrheadings}
\manualmark

\newcommand{\bmm}[1]{{\mathbf{#1}}}

\ifoot[DTU Wind Energy ????]{DTU Wind Energy ????}

\graphicspath{{./figures/}}


\usepackage{totcount}
\newtotcounter{citnum} %From the package documentation
\def\oldbibitem{} \let\oldbibitem=\bibitem
\def\bibitem{\stepcounter{citnum}\oldbibitem}

\newtotcounter{fignum}
\def\oldfigure{} \let\oldfigure=\figure
\def\figure{\stepcounter{fignum}\oldfigure}

\newtotcounter{tablenum}
\def\oldtable{} \let\oldtable=\table
\def\table{\stepcounter{tablenum}\oldtable}

\regtotcounter{page}


\begin{document}
%\includepdf[pages=1]{./report_forside.pdf}
\include{frontpage}
\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Introduction}
Operation of wind turbines at high wind speed, above the standard cut-out wind speed, can be a highly appreciated feature. A smooth shut-down for increasing wind speed can guarantee a continous power production avoiding abrupt shut-down when a high-wind front is passing through a wind farm.

Load analysis and control strategies for high wind conditions have been the focus of several previous investigations.
Markou and Larsen~\cite{markou_control_2009} investigate two controller strategies for above cut-out conditions. In the first strategy, the reference rotational speed and power are kept constant. In the second strategy the reference rotational speed and power are decreased for increasing wind speeds. Active longitudinal and lateral tower dampers are required to reduce tower loads. Results from nonlinear aeroservoelastic simulations show that both strategies do not lead to significant increases in fatigue loads compared to operation up-to cut-out and then idling.

Bossanyi and King~\cite{bossanyi_improving_2012} also investigate the problem of above cut-out strategies. In their work they reduce both the rotor speed and the generator torque reference from~\SI{23}{m/s} till~\SI{35}{m/s}. Their controller includes a speed exclusion zone to avoid resonance of the tower mode frequency with the blade passing frequency (3P). A lateral tower damper is also included to avoid excessive increase in the tower lateral vibrations.

Tibaldi and Hansen~\cite{tibaldi_aeroservoelastic_2016} analyze the stability of a turbine operating at high wind speed. In their work, they look at frequencies and dampings of a turbine operating up to \SI{45}{m/s} both in open loop and closed-loop.

This work is a continuation of the one presented by Tibaldi and Hansen~\cite{tibaldi_aeroservoelastic_2016}. Here the performances of the wind turbine are analyzed in time domain through aeroservoelastic simulations performed with HAWC2~\cite{larsen_how_2014,passon_oc3benchmark_2007,kim_development_2013}.
A modified version of the basic DTU Wind Energy controller~\cite{Hansen_Basic_2013} is used to allow derating of the turbine based on pitch feedback.

The analysis is performed using the NREL 5MW Reference Wind Turbine (NREL) and the 5MW wind turbine specifically designed for the Off Wind China Project (OWC).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Controller}
The controller used in this investigation is a modified version of the basic DTU Wind Energy controller\cite{Hansen_Basic_2013}. The controller has been changed to allow derating and to have a more general gain-scheduling of the pitch PI controller.

Figure~\ref{fig:controller} shows a graphical representation of the derating feature of the controller.

The derating is implemented by changing the rotor speed $\Omega$ and generator torque $Q$ reference values as a function of the measured pitch angle $\beta$. The measured pitch angle is filtered through two first-order low-pass filters; one for the rotor speed with time constant $\tau_{\Omega}$, and one for the generator torque, with time constant $\tau_{W}$. Both rotor speed and generator torque are decreased linearly with two different functions, characterized by the constants $K_{\Omega, 0}$, $K_{\Omega, 1}$ and $K_{Q, 0}$, $K_{Q, 1}$.

A new gain scheduling of the pitch PI controller has been implemented to allow for a more freedom in the controller tuning at high wind speed. The new scheduling is based on a look-up table that is linearly interpolated as a function of the low-passed-filtered pitch angle, with time constant $\tau_{gs}$.

This scheme, with all the features already existing in the basic DTU Wind Energy Controller, allows for a smooth reduction of the rotor speed and generator torque when the wind speed increases above the normal cut-out wind speed.

\begin{figure}[!b]
\centering
\includegraphics[width=0.7\textwidth]{controller.eps}
\caption{\label{fig:controller} Diagram of the controller feedbacks to reduce the reference rotor speed and the generator torque.}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Derating strategy and steady-states analysis}
This section describes the strategy that is used to derate the turbine through a steady-states analysis. The frequencies and damping of the aeroelastic modes are reported to give a first insight of the wind turbine response. This analysis is performed with HAWCStab2~\cite{hansen_aeroelastic_2011, hansen_aeroelastic_2004, sonderby_open-loop_2014, hansen_improved_2003}. The procedure to tune the controller is also described in this section.

In this analysis, the operational steady-states values are obtained with a derate that is linear with respect to the wind speed and not the pitch angle. This has been done for simplicity of the implementation. This difference leads to a mismatch between the operational points that would be obtained with the pitch feedback when the rotor speed reached its minimum value and therefore the pitch angle increases with a different slope for increasing wind speeds. 

Figure~\ref{fig:rot_torque} shows the steady states values of the rotor speed and generator torque from \SI{4}{} to \SI{45}{m/s}, for the NREL 5MW RWT and the Off Wind China 5MW turbine. Note that, above \SI{25}{m/s} the rotor speed decreases linearly until it reaches the minimum rotor speed value. The minimum rotor speed value has been selected to avoid resonances between the 6P excitation and the edgewise whirling modes of the OWC model. The slope of the rotor speed has been set such that it would reach 20\% of the rated vale at \SI{45}{m/s} if it would not be limited by the minimum rotor speed constant. The generator torque, on the other hand, decreases until the wind speed of \SI{45}{m/s}, where it reaches 20\% of its value at rated wind speed.

\begin{figure}[!b]
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{rotor_speed.eps}
\center\emph{a)~Rotor speed.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{torque.eps}
\center\emph{b)~Generator torque.}
\end{minipage}
\caption{\label{fig:rot_torque} Steady states rotor speed and generator torque of the NREL 5MW RWT (labeled NREL with blue lines) and of the Off Wind China 5MW turbine  (labeled OWC with green lines).}
\end{figure}

\begin{figure}[!t]
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{pitch.eps}
\center\emph{a)~Pitch angle.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{power.eps}
\center\emph{b)~Generator power.}
\end{minipage}
\caption{\label{fig:pitch_power} Steady states pitch angle and generator power of the NREL 5MW RWT (labeled NREL with blue lines) and of the Off Wind China 5MW turbine  (labeled OWC with green lines).}
\end{figure}

Figure~\ref{fig:pitch_power} shows the steady-states values of the pitch angle and the aerodynamic power, for the same turbines. The pitch angle at \SI{45}{m/s} has a value of more than \SI{50}{deg}. The power decreases smoothly for increasing wind speed.

%%%%%%%
\section{Stability analysis}
\begin{figure}[!b]
\centering
\includegraphics[width=\textwidth]{frequency.eps}
\caption{\label{fig:frequencies} Frequencies of the first seven wind turbine aeroelastic modes of the NREL 5MW RWT (labeled NREL circles) and of the Off Wind China 5MW turbine (labeled OWC stars) from \SI{20}{} to \SI{45}{m/s}. External excitation in dashed black lines (3P and 6P).}
\end{figure}

Figure~\ref{fig:frequencies} shows the frequencies of the first seven aeroelastic wind turbines modes.The main difference between the two models is in the first blade edgewise freqeuncy. The OWC turbine has the frequencies of the first backward whirling and first forward whirling edgewise modes that are lower than those of the NREL model. The NREL model has a resonance of the first forward edgewise mode with the 6P excitation at rated rotational speed and of the first backward whirling edgewise with the 6P when the rotor speed reached the minimum value at \SI{34}{m/s}. The OWC model has the freqeuncy of the first forward whirling edgewise mode that crosses the 6P excitation at approximately \SI{28}{m/s}.


Figure~\ref{fig:damping} shows the damping of the first seven aeroelastic wind turbines modes. Above \SI{25}{m/s}, all the shown modes of OWC have lower damping than those of the NREL model. The lowest damped modes, first tower lateral and first forward and backward whirling edgewise modes, have a damping that is approximately \SI{0.5}{\%} lower at most wind speed above standard cut-out.

\begin{figure}[!b]
\centering
\includegraphics[width=\textwidth]{damping.eps}
\caption{\label{fig:damping} Damping of the first seven wind turbine aeroelastic modes of the NREL 5MW RWT (labeled NREL circles) and of the Off Wind China 5MW turbine (labeled OWC stars) from \SI{20}{} to \SI{45}{m/s}.}
\end{figure}

Figure~\ref{fig:alpha} shows the distribution of the angle of the attack along the blades of the NREL and OWC models at \SI{30}{m/s}. The blades are at a negative angle of attack for almost half of their extension. However, both blades are still away from negative stall, that occurs, for the tip sections at about \SI{-15}{deg}.  

\begin{figure}[!b]
\centering
\includegraphics[width=0.49\textwidth]{alpha.eps}
\caption{\label{fig:alpha} Angle of attack along the blades of the NREL model and OWC model at \SI{30}{m/s}.}
\end{figure}

Figure~\ref{fig:mode_shape} shows the flapwise and edgewise contribution to the first aeroelastic blade flapwise mode and first aeroelastic blade edgewise mode. Both models are compared. The plots show that the NREL model has the first flapwise mode with a higher edgewise contribution and the first edgewise mode with a first flapwise contribution compared to the OWC model. The fact that there is more flapwise motion in the edgewise mode explain the higher damping of the edgewise whirling modes of the NREL model shown in Figure~\ref{fig:damping}.
\begin{figure}[!t]
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{blade_flap.eps}
\center\emph{a)~First blade flapwise mode.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{blade_edge.eps}
\center\emph{b)~First blade edgewise mode.}
\end{minipage}
\caption{\label{fig:mode_shape} Aeroelastic mode shape of the first blade flapwise and edgewise mode at \SI{30}{m/s}. Comparison between the NREL 5MW RWT (labeled NREL in blue) and of the Off Wind China 5MW turbine (labeled OWC in red). }
\end{figure}

%%%%%%%
\section{Controller tuning}
From Figures \ref{fig:rot_torque} and \ref{fig:pitch_power} the relationship between the pitch angle and the rotor speed and the pitch angle and the generator torque can be retrieved to identify the parameters to tune the derating scheduling. Since the pitch angle of the two models is slightly different also the tuning differs. Table~\ref{tab:derating} shows the coefficients of the linear derating that are used for the two models.
The time constants of the pitch first-order low-pass filters to reduce the rotor speed and and generator torque are set both to \SI{15}{s}.

The gains of the pitch PI controller are computed using a pole placement technique described in \cite{hansen_control_2005, tibaldi_effects_2014}. The pole is placed at a frequency of \SI{0.06}{Hz} with \SI{70}{\%} damping. Since the two models have different aerodynamic performances the resulting gains of the two models are different.

\begin{table}[!b]
\centering
\caption{\label{tab:derating} Coefficients used for the derating of the rotor speed and torque.}
\begin{tabular}{lcc}
\hline
				& NREL		&	OWC	\\
\hline
$K_{\Omega, 0}$	[rad/s]&	1.709 		&	1.738	\\
$K_{\Omega, 1}$	[1/s]	&	-1.194	&	-1.209\\
$K_{Q, 0}$		[MNm]	&	5.296 		&	5.388\\
$K_{Q, 1}$	[MNm/rad]	&	-3.772	&	-3.819\\
\hline
\end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Loads analysis}
This section shows a load analysis performed with the aeroservoelastic code HAWC2. A set of simulation, corresponding to the DLC 1.2 \cite{iec/tc88_iec_2005}, is evaluated with wind speed ranging from \SI{4}{} to \SI{44}{m/s} with \SI{2}{m/s} step. Three yaw angles, $0$ and $\pm$ \SI{10}{deg}, are included and six turbulence seed for each wind speed and yaw angle, resulting in 378 simulations. The wind conditions are conforming with the wind class II C. The wind shear is set with a roughthness of \SI{0.001}{}.

\section{Statistics}

\begin{figure}[!h]
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{stats_pitch.eps}
\center\emph{a)~Pitch angle.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{stats_rotor.eps}
\center\emph{b)~Rotor speed.}
\end{minipage}
\center
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{stats_power.eps}
\center\emph{c)~Generator power.}
\end{minipage}
\caption{\label{fig:loads_stats} Pitch angle, rotor speed, and generator torque statistics of the NREL 5MW RWT (labeled NREL in blue) and of the Off Wind China 5MW turbine (labeled OWC in red).}
\end{figure}

Figure~\ref{fig:loads_stats} shows the statistics of the pitch angle, rotor speed, and generator power for the NREL and OWC models. The plots show mean, maximum, and minimum values at each wind speed. Both turbines operate above the standard cut-out wind speed of \SI{25}{m/s} upt to \SI{44}{m/s}. Between \SI{25}{} and \SI{28}{m/s} the pich angle increases very quicly because both the rotor speed and the generator torque are reduced due to the derating. Above \SI{28}{m/s} the rotor speed does not decrease because it has reached the minimum rotor speed value to avoid resonace with tower modes. At \SI{44}{m/s} the turbines are operating at the minimum rotor speed with a pitch angle of \SI{50}{deg} and producing \SI{1.7}{MW}.


Figure~\ref{fig:loads_stats2} shows the statistics of the blade root and tower base bending moments for the NREL and OWC models. From the plot of the blade root edgewise bending moment it appears clearly that the OWC model is occurring in significant edgewise vibrations for wind speeds above \SI{28}{m/s}. The highest component that contributes to the vibrations corresponds to the forward edgewise whirling mode. Despite the OWC model has the edgewise modes further away from the external excitations than those of the NREL model (see Figure \ref{fig:frequencies}), the lower damping of the modes (see Figure \ref{fig:damping}) leads to higher vibrations. In the flapwise direction the moments increase steadily for the increasing wind speed. At the tower base the statistics in the lateral direction of the two model are similar, while in the longitudinal direction higher loads for the OWC model can be noticed above \SI{30}{m/s}.

\begin{figure}[!b]
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{stats_blade_edge.eps}
\center\emph{a)~Blade root edgewise.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{stats_blade_flap.eps}
\center\emph{b)~Blade root flapwise.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{stats_twr_lat.eps}
\center\emph{c)~Tower base lateral.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{stats_twr_long.eps}
\center\emph{d)~Tower base longitudinal.}
\end{minipage}
\caption{\label{fig:loads_stats2} Blade root and tower base bending moments statistics of the NREL 5MW RWT (labeled NREL in blue) and of the Off Wind China 5MW turbine (labeled OWC in red).}
\end{figure}

\section{Fatigue}
Figure~\ref{fig:loads_del} shows the fatigue damage equivalent loads at blade root and tower base for the NREL and OWC models. The blade edgewise load shows clearly that vibrations are occurring above \SI{28}{m/s} for the OWC model. The fatigue loads of OWC in the flapwise direction are constantly above those of the NREL model. Below \SI{28}{m/s} because of the different pitch angle that leads to a higher gravitational contribution. Above \SI{28}{m/s} due to the vibrations of the first edgewise whirling mode. Above the standard cut-out wind speed the flapwise fatigue load increases significantly also for the NREL model. The damage equivalent load of the flapwise component at \SI{44}{m/s} is approximately \SI{60}{\%} higher than the one at \SI{24}{m/s}. At the tower base the fatigue loads are also significantly higher for the OWC model. Unlike in the statistics, the fatigue damage at the tower base, clearly illustrates the presence of vibrations. Also at the tower base the fatigue loads significantly increase above \SI{25}{m/s}, even when no violent vibrations occur.


\begin{figure}[!b]
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{del_blade_edge.eps}
\center\emph{a)~Blade root edgewise.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{del_blade_flap.eps}
\center\emph{b)~Blade root flapwise.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{del_twr_lat.eps}
\center\emph{c)~Tower base lateral.}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=\textwidth]{del_twr_long.eps}
\center\emph{d)~Tower base longitudinal.}
\end{minipage}
\caption{\label{fig:loads_del} Blade root and tower base bending moments fatigue damage equivalent  loads of the NREL 5MW RWT (labeled NREL in blue) and of the Off Wind China 5MW turbine (labeled OWC in red).}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Conclusions}
A controller for operation above the traditional cut-out wind speed has been described and implemented. The controller allows derating of the reference rotor speed and reference generator torque. 

A strategy for operation at high wind speed has been analyzed for two wind turbine models, both with steady states calculation and dynamic aeroelastic simulations. The simulations show that the OWC model incurs into edgewise vibrations due to very lowly damped wind turbine edgewise whirling modes. The low damping is due to the structural blade design that has a lower coupling between the blade and edgewise directions, hence in the edgewise modes there is less flapwise motion that would increase the damping.

The analysis show that both fatigue and maximum loads increase also for the NREL model as the wind speed increases. The results are aligned with the findings of previous investigations.

%%%%%%%%%%%%%
\bibliographystyle{unsrt}
%\bibliography{./../BiblioDatabase}
\begin{thebibliography}{10}

\bibitem{markou_control_2009}
Helen Markou and Torben~J. Larsen.
\newblock Control strategies for operation of pitch regulated turbines above
  cut-out wind speeds.
\newblock {\em EWEC 2009 Proceedings online}, 2009.

\bibitem{bossanyi_improving_2012}
E~Bossanyi and J~King.
\newblock Improving wind farm output predictability by means of a soft cut-out
  strategy.
\newblock Copenhagen, 2012.

\bibitem{tibaldi_aeroservoelastic_2016}
C.~Tibaldi and M.H. Hansen.
\newblock Aeroservoelastic analysis of storm-ride-through control strategies
  for wind turbines.
\newblock 2016.

\bibitem{larsen_how_2014}
Torben~J. Larsen and Anders~Melchior Hansen.
\newblock How 2 {HAWC}2, the user's manual.
\newblock Technical Report Risø-R-1597(ver. 4-5)(EN), Risø National
  Laboratory, 2014.

\bibitem{passon_oc3benchmark_2007}
P.~Passon, M.~Kühn, S.~Butterfield, J.~Jonkman, T.~Camp, and T.~J. Larsen.
\newblock {OC}3—{Benchmark} {Exercise} of {Aero}-elastic {Offshore} {Wind}
  {Turbine} {Codes}.
\newblock {\em Journal of Physics: Conference Series}, 75(1):012071, July 2007.

\bibitem{kim_development_2013}
Taeseong Kim, Anders~Melchior Hansen, and Kim Branner.
\newblock Development of an anisotropic beam finite element for composite wind
  turbine blades in multibody system.
\newblock {\em Renewable Energy}, 59:172--183, 2013.

\bibitem{Hansen_Basic_2013}
Morten~Hartvig Hansen and Lars~Christian Henriksen.
\newblock Basic {DTU} {Wind} {Energy} controller.
\newblock Technical Report E-0028, DTU Wind Energy, Roskilde, Denmark, 2013.

\bibitem{hansen_aeroelastic_2011}
M.~H. Hansen.
\newblock Aeroelastic {Properties} of {Backward} {Swept} {Blades}.
\newblock pages 1--19, Orlando, Florida, 2011. American Institute of
  Aeronautics and Astronautics.

\bibitem{hansen_aeroelastic_2004}
M.~H. Hansen.
\newblock Aeroelastic stability analysis of wind turbines using an eigenvalue
  approach.
\newblock {\em Wind Energy}, 7(2):133--143, 2004.

\bibitem{sonderby_open-loop_2014}
Ivan Sønderby and Morten~H. Hansen.
\newblock Open-loop frequency response analysis of a wind turbine using a
  high-order linear aeroelastic model.
\newblock {\em Wind Energy}, 17(8):1147--1167, 2014.

\bibitem{hansen_improved_2003}
M.~H. Hansen.
\newblock Improved {Modal} {Dynamics} of {Wind} {Turbines} to {Avoid}
  {Stall}-induced {Vibrations}.
\newblock {\em Wind Energy}, 6(2):179--195, 2003.

\bibitem{hansen_control_2005}
Morten~H. Hansen, Anca Hansen, Torben~J. Larsen, Stig Øye, Poul Sørensen, and
  Peter Fuglsang.
\newblock Control design for a pitch-regulated, variable speed wind turbine.
\newblock Technical Report Risø-R-1500(EN), Risø National Laboratory,
  Roskilde, Denmark, 2005.

\bibitem{tibaldi_effects_2014}
Carlo Tibaldi, Lars~Christian Henriksen, Morten~H. Hansen, and Christian Bak.
\newblock Effects of gain-scheduling methods in a classical wind turbine
  controller on wind turbine aero-servo-elastic modes and loads.
\newblock In {\em 32nd {ASME} {Wind} {Energy} {Symposium}}, pages 1--12.
  American Institute of Aeronautics and Astronautics, 2014.

\bibitem{iec/tc88_iec_2005}
IEC/TC88.
\newblock {\em {IEC} 61400-1 {Ed}.3: {Wind} turbines - {Part} 1: {Design}
  requirements}.
\newblock 2005.

\end{thebibliography}

%\includepdf[pages=4]{./report_forside.pdf}
\end{document}